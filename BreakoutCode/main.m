//
//  main.m
//  BreakoutCode
//
//  Created by James Cash on 08-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dog.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...

        Dog *dog = [[Dog alloc] init];
        NSLog(@"Our dog: %@", dog);
        // can't do either of the below:
//        dog.name;
//        dog._name;
        // because instance variables are private to the object -- only the object itself can access them in its methods

        // now, after adding our "getter" method to Dog, we can do
        NSLog(@"Dog's name is: %@", [dog name] /* dog.name */);

        // Note: _name is the instance variable, the actual data that's stored on the object
        // and "name" (in "dog.name") is actually a method that returns the value of what's stored in _name

        dog.name = @"Fido";
        dog.breed = @"terrier";
        NSLog(@"Dog's name is now %@", dog.name);

    }
    return 0;
}

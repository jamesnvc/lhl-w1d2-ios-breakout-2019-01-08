//
//  Dog.m
//  BreakoutCode
//
//  Created by James Cash on 08-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "Dog.h"

@implementation Dog

- (instancetype)init
{
    self = [super init];
    if (self) {
        _name = @"Rover";
        _breed = @"Lab";
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"A good dog named %@", _name];
}

- (NSString *)name
{
    NSLog(@"accessing dog's name");
    return _name;
}

// dog.name = aoeu
- (void)setName:(NSString *)newName
{
    NSLog(@"Changing name from %@ to %@", _name, newName);
    _name = newName;
}

- (void)setBreed:(NSString *)breed
{
    NSLog(@"Changing breed from %@ to %@", self.breed, breed);
//    self.breed = breed;
//    [self setBreed:breed];
    _breed = breed;
}

@end

//
//  Dog.h
//  BreakoutCode
//
//  Created by James Cash on 08-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Dog : NSObject {
    // this is declaring an instance variable for the Dog class
    // meaning that every Dog object that gets alloc'd will have storage space for an NSString* and will be able to refer to that NSString* by the name "_name" inside method bodies
    NSString* _name;
}

- (NSString*)name;
- (void)setName:(NSString*)newName;

@property (nonatomic,strong) NSString* breed;

@property (nonatomic,assign) NSInteger x;

@end

NS_ASSUME_NONNULL_END
